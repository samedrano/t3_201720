package model.data_structures.test;

import model.data_structures.Stack;
import junit.framework.TestCase;

public class StackTest extends TestCase
{
	private Stack<String> pila;
	
	//--------------------------------------------
	//Escenarios
	//-------------------------------------------- 

	private void setupEscenario1()
	{
		pila = new Stack<String>();
		
		pila = new Stack<String>();
		pila.push("prueba1");
		pila.push("prueba2");
		pila.push("prueba3");
		pila.push("prueba4");
	}
	private void setupEscenario2()
	{
		pila = new Stack<String>();
	}

	//--------------------------------------------
	//Metodos
	//--------------------------------------------
	
	public void testPush()
	{
        setupEscenario1();
		
		pila.push("n");
		assertEquals("No es el tama�o correcto",5,pila.size());
	}
	public void testPop()
	{
		setupEscenario1();
		
		String n = pila.pop();
		assertEquals("No se elimino el elemento correcto", n, "prueba4");
		assertEquals("No es el tama�o correcto", pila.size(), 3);
	}
	public void testIsEmpty()
	{
		setupEscenario1();		
		assertEquals("La cola no esta vacia", pila.isEmpty(), false);
		
		setupEscenario2();		
		assertEquals("La cola esta vacia", pila.isEmpty(), true);
	}
	public void testSize()
	{
		setupEscenario1();
		assertEquals("No es el tama�o correcto", pila.size(), 4);
		
		setupEscenario2();
		assertEquals("No es el tama�o correcto", pila.size(), 0);
	}
}
