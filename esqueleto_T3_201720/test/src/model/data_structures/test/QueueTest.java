package model.data_structures.test;

import model.data_structures.Queue;
import junit.framework.TestCase;


public class QueueTest extends TestCase
{
	private Queue<String> cola;
	
	//--------------------------------------------
	//Escenarios
	//-------------------------------------------- 

	private void setupEscenario1()
	{
		cola = new Queue<String>();
		cola.enqueue("prueba1");
		cola.enqueue("prueba2");
		cola.enqueue("prueba3");
		cola.enqueue("prueba4");
		
	}
	private void setupEscenario2()
	{
		cola = new Queue<String>();
	}

	//--------------------------------------------
	//Metodos
	//-------------------------------------------- 

	public void testEnqueue()
	{
		setupEscenario1();
		
		cola.enqueue("n");
		assertEquals("No es el tama�o correcto", cola.size(), 5);
	}
	public void testDequeue()
	{
		setupEscenario1();
		
		String n = cola.dequeue();
		assertEquals("No se elimino el elemento correcto", n, "prueba1");
		assertEquals("No es el tama�o correcto", cola.size(), 3);
	}
	public void testIsEmpty()
	{
		setupEscenario1();		
		assertEquals("La cola no esta vacia", cola.isEmpty(), false);
		
		setupEscenario2();		
		assertEquals("La cola esta vacia", cola.isEmpty(), true);
	}
	public void testSize()
	{
		setupEscenario1();
		assertEquals("No es el tama�o correcto", cola.size(), 4);
		
		setupEscenario2();
		assertEquals("No es el tama�o correcto", cola.size(), 0);
	}
}
