package model.logic;

public class StopTimes implements Comparable<StopTimes>{

private int trip_id;
private String arrival_time;
private String departure_time;
private int stopID;
private int stopSequence;
private String stopHeadSign;
private int pickupType;
private int dropOfftype;
private double shapeDistTravel;

public StopTimes(int pID, String pArrivalTime, String pDepartureTime, int pStopID, int pStopSequence,String pSth, int pPickUpType,int pDropoffTypet,double pShapeDist){
	trip_id=pID;
	arrival_time=pArrivalTime;
	departure_time=pDepartureTime;
	stopID=pStopID;
	stopSequence=pStopSequence;
	stopHeadSign=pSth;
	pickupType=pPickUpType;
	dropOfftype=pDropoffTypet;
	shapeDistTravel=pShapeDist;
}

public int darTripID(){
	return trip_id;
}

public String darArrivalTime(){
	return arrival_time;
}
public String darDepartureTime(){
	return departure_time;
}
public int darStopID(){
	return stopID;
}
public int darStopSequence(){
	return stopSequence;
}
public String darStopHeadSign(){
	return stopHeadSign;
}
public int darPickUpType(){
	return pickupType;
}
public int darDropoffType(){
	return dropOfftype;
}
public double darShapeDistTravel(){
	return shapeDistTravel;
}



	
	@Override
	public int compareTo(StopTimes o) {
		// TODO Auto-generated method stub
	if(trip_id==o.darTripID()&& stopID==o.darStopID()&arrival_time==o.darArrivalTime()){
		return 0;
	}else{
		return -1;
	}
		
		
	}

}
