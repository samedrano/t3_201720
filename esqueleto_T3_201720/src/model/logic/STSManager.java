package model.logic;

import java.io.BufferedReader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import model.data_structures.DoubleLinkedList;
import model.data_structures.IStack;
import model.data_structures.ListaCircular;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.vo.BusUpdateVO;
import model.vo.StopVO;
import api.ISTSManager;

public class STSManager implements ISTSManager{
	//Separador
	public static final String SEPARADOR=",";
	private Queue<BusUpdateVO>lista;
	private ListaCircular<Stops> stops;
	private DoubleLinkedList<StopTimes> stopTimes;
	
	//El carga los archivos pero la generación de la ruta por parte del controlador esta erronea
	
	public void readBusUpdate(File rtFile) {
		BufferedReader reader=null;
		FileReader in = null;
		try {
			in=new FileReader(rtFile);
			reader = new BufferedReader(in);
			Gson gson= new GsonBuilder().create();
			lista= new Queue<BusUpdateVO>();
			BusUpdateVO[] arreglo=gson.fromJson(reader, BusUpdateVO[].class);
			
			
			for (int i = 0; i < arreglo.length; i++) {
				lista.enqueue(arreglo[i]);
			}
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			
		}finally{
			try {
				System.out.println(rtFile);
				reader.close();
				in.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	// Si cargo las paradas que ruta le paso ? 

	public IStack<StopVO> listStops(Integer tripID) {
		// TODO Auto-generated method stub
		boolean ya =false;
		double latitud1=0;
		double longitud1=0;
		double latitud2=0;
		double longitud2=0;
		
		Stack<StopVO> listaParadas=new Stack<StopVO>();
		//cargo los stops para coger la longitud y latitud, despues cogo el trip que me importa y lo cargo y si la distancia que pasa el bus entonces lo agrego.
		for (int i = 0; i < lista.size()&& ya==false; i++) {
			BusUpdateVO este=lista.darElemento(i).darInformacion();
			if (este.dartripID()==tripID){
				ya=true;
				latitud1= este.darLatitud();
				 longitud1=este.darLongitud();
			}
		}
		
		DoubleLinkedList<Integer> stopsID= new DoubleLinkedList<Integer>();
		
		for (int i = 0; i < stopTimes.getSize(); i++) {
			
			
			if(stopTimes.getElementAtK(i).darTripID()==tripID){
				stopsID.addAtEnd(stopTimes.getElementAtK(i).darStopID());
				
			}
		
		}
		
	
		for (int j = 0; j < stopsID.getSize(); j++) {
			
		
				for (int i = 0; i < stops.getSize(); i++) {
					if(stops.getElementAtK(i).darStopId()==stopsID.getElementAtK(j)){
						latitud2= stops.getElementAtK(j).darStopLat();
						longitud2= stops.getElementAtK(j).darStopLon();
						double aComparar = getDistance(latitud1, longitud1, latitud2 , longitud2);
						if(aComparar<=70){
							StopVO nuevo= new StopVO(stops.getElementAtK(i).darStopName());
							listaParadas.push(nuevo);
						}
					}
				}
		
		
		}
		
		return listaParadas;
	}


	
	public double getDistance(double lat1, double lon1, double lat2, double lon2) {
		
		 int R = 6371*1000; // Radious of the earth
	
		Double latDistance = toRad(lat2-lat1);
		Double lonDistance = toRad(lon2-lon1);
		Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + 
		Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) * 
		Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
		Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		Double distance = R * c;
		return distance;
		}

		  private Double toRad(Double value) {
			  return value * Math.PI/180;
		  }

		public void loadStops() {
			 try( FileReader reader = new FileReader( "data/stops.txt");
			           BufferedReader in = new BufferedReader( reader );){
					 int contador=0;
			        String linea = in.readLine( );

			      while(linea!=null){
			    	  if(contador==0){
			    		  linea=in.readLine();
			    	  }else{
			    		  contador++;
			    		  	String[] partir = linea.split(SEPARADOR);
			    		// Asigno los atributos  
			    		  
			    		   int stopID=Integer.parseInt(partir[0]);
			    			 int stopCode=Integer.parseInt(partir[1]);
			    			 String stopName=partir[2];
			    			 String stopDesc=partir[3];
			    			 double stopLat=Long.parseLong(partir[4]);
			    			 double stopLon=Long.parseLong(partir[5]);
			    			 String zoneID=partir[6];
			    			 String stopURL=partir[7];
			    			 int locationType=Integer.parseInt(partir[8]);
			    			 String parentStation=partir[9];
			    		  
			    		  // Creo la nueva info del nodo y lo agrego
			    		 Stops nuevo= new Stops(stopID,stopCode,stopName,stopDesc,stopLat,stopLon,zoneID,stopURL,locationType,parentStation);
			    		  	
			    		 stops.AgregarEnOrdenAscendente(nuevo);
			    		  linea=in.readLine();
			    	  }
			    	  
			      }
			        
				 }catch (Exception e){
					 e.printStackTrace();
				 }
				// TODO Auto-generated method stub
			
			// TODO Auto-generated method stub
			
		}

	
	
}
