package model.vo;

public class BusUpdateVO implements Comparable<BusUpdateVO>{
	private int vehicleNo;
	private long tripID;
	private int routeNo;
	private String direction;
	private String destination;
	private String pattern;
	private double latitude;
	private double longitud;
	private String recordedTime;
	private String routeMap;
	
	public  BusUpdateVO(int pv, long tI, int rNO, String dir, String des, String pat, double lat, double lon, String rec, String rout){
		
		vehicleNo=pv;
		tripID=tI;
		routeNo=rNO;
		direction=dir;
		destination=des;
		pattern=pat;
		latitude=lat;
		longitud=lon;
		recordedTime=rec;
		routeMap=rout;
		
		
	}
	
	



	public int darVehicleNo(){
		return vehicleNo;
	}
	public long dartripID(){
		return tripID;
	}
	public int darRouteNo(){
		return routeNo;
	}
	public String darDirection(){
		return direction;
	}
	public String darDestination(){
		return destination;
	}
	public String darPattern(){
		return pattern;
	}
	public double darLongitud(){
		return longitud;
	}
	public double darLatitud(){
		return latitude;
	}
	public String darRecordedTime(){
		return recordedTime;
	}
	public String darRouteMap(){
		return routeMap;
	}
	
	
	
	
	public int compareTo(BusUpdateVO o) {
		// TODO Auto-generated method stub
		return vehicleNo-o.darVehicleNo();
	}

}
