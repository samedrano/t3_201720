package model.data_structures;

public class Queue<T extends Comparable<T>> implements IQueue<T>
{
	private int size;
	private DoubleLinkedList<T> lista;
	private Nodo<T> primero;
	private Nodo<T> ultimo;
	private Nodo<T> actual;
	public Queue()
	{
		lista = new DoubleLinkedList<T>();
		size = 0;
	}
	
	public void enqueue(T item) {
		
		Nodo<T> agregar=new Nodo<T>(item);
		
		if(primero==null){
			lista.addAtEnd(item);
			size++;
			primero=agregar;
			ultimo=agregar;
		}else{
			ultimo =agregar;
			lista.addAtEnd(item);
			size++;
		}
		
	}
	
	public T dequeue() {
		if (size >0)
		{
			T dato =lista.getElementAtK(0);
			lista.deleteAtK(0);
			size--;
			return dato;
		}
		return null;
	}	
	
	public Nodo<T> darElemento(int pos){
		int contador=0;
		actual=primero;
		while(contador<pos){
			
			next();
			
			contador++;
		}
		
		return actual;
		
		
	}
	
	public Nodo<T> darActual(){
		return actual;
	}
	public Nodo<T> next(){
		return actual=actual.darSiguiente();
	}
	
	
	public boolean isEmpty()
	{
		boolean s = size == 0 ? true : false;
		return s;
	}
	public int size()
	{
		return size;
	}
}
