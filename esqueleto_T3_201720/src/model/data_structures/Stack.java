package model.data_structures;

public class Stack<T extends Comparable<T>> implements IStack<T>
{
	private int size;
	private DoubleLinkedList<T> lista;
	private int top;
	
	public Stack()
	{
		lista = new DoubleLinkedList<T>();
		size = 0;
	}

	public void push(T item) {
		lista.addAtEnd(item);
		size++;	
	}

	public T pop() {
		if ( size >0 )
		{
			T dato = lista.getElementAtK(size - 1);
			lista.deleteAtK(size - 1);
			size--;
			return dato;
		}
		return null;
	}
	public boolean isEmpty()
	{
		// si size == 0, s es true, de la contrario es false
		boolean s = size == 0 ? true : false;
		return s;
	}
	public int size()
	{
		return size;
	}

}
