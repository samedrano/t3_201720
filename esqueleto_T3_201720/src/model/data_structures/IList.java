package model.data_structures;

/**
 * Abstract Data Type for a list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface IList<T> extends Iterable<T> {

	public int getSize();
	
	public void add( T elemento);
	
	public void addAtEnd( T elemento);
	
	public boolean delete();
	
	public T getElement();
	
	public void deleteAtK(int k);
	
	public void addAtk(T elemento, int k);
	
	public T getElementAtK(int k);
	
	public void next();
	
	public void previous();
	
}
